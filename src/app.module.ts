import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {join} from "path";
import {ClientsModule, Transport} from "@nestjs/microservices";

@Module({
  imports: [ClientsModule.register([
    {
      name: 'AUTH_PACKAGE',
      transport: Transport.GRPC,
      options: {
        package: 'auth',
        protoPath: join(__dirname, '../src/proto/auth.proto'),
        url: 'authservice-service:5000',
      }
    }
  ])],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
