import {Inject, Injectable, OnModuleInit} from '@nestjs/common';
import {ClientGrpc} from "@nestjs/microservices";
import {Observable} from "rxjs";


interface AuthService {
  userById(data: {id: string}): Observable<any>;
}

@Injectable()
export class AppService implements OnModuleInit{
  private authService: AuthService;

  constructor(@Inject('AUTH_PACKAGE') private client: ClientGrpc) {}

  onModuleInit() {
    this.authService = this.client.getService<AuthService>('AuthService');
  }

  getUserById(): Observable<string> {
    return this.authService.userById({id: '1354d22f-4ccd-4005-bb66-7404b489b933'});
  }
}
