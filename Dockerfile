FROM node:13 AS builder
WORKDIR /app
COPY ./package.json ./
RUN yarn
COPY . .
RUN yarn build

FROM node:13
WORKDIR /app
COPY --from=builder /app ./
EXPOSE 3000
ENTRYPOINT ["yarn", "start:prod"]
